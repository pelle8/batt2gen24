sudo apt install mosquitto python3-full can-utils pip apache2 git mosquitto-clients tmux
cd /opt
sudo git clone https://gitlab.com/pelle8/batteryemulator.git
sudo ln -s /opt/batteryemulator/ /opt/batt2gen24
sudo systemctl enable mosquitto
echo Type twise: batt2gen24_pass
sudo mosquitto_passwd -c /etc/mosquitto/.passwd batt2gen24
sudo mkdir /etc/mosquitto/conf.d/
sudo touch /etc/mosquitto/conf.d/auth.conf
sudo chmod 666 /etc/mosquitto/conf.d/auth.conf
sudo echo listener 1883 >/etc/mosquitto/conf.d/auth.conf
sudo echo allow_anonymous false>>/etc/mosquitto/conf.d/auth.conf
sudo echo password_file /etc/mosquitto/.passwd>>/etc/mosquitto/conf.d/auth.conf
sudo touch /etc/mosquitto/conf.d/websockets.conf
sudo chmod 666 /etc/mosquitto/conf.d/websockets.conf
sudo echo listener 9001 >/etc/mosquitto/conf.d/websockets.conf
sudo echo protocol websockets>>/etc/mosquitto/conf.d/websockets.conf
sudo systemctl restart mosquitto

cd /opt/batteryemulator
sudo chmod +x *.sh
sudo pip install -r requirements.txt --break-system-packages
#sudo pip install psutil --break-system-packages

sudo mkdir /etc/batt2gen24
sudo cp /opt/batteryemulator/bms_config_batrium.yaml.example /etc/batt2gen24/bms_config_batrium.yaml
sudo cp /opt/batteryemulator/bms_config_juntek.yaml.example /etc/batt2gen24/bms_config_juntek.yaml
sudo cp /opt/batteryemulator/bms_config_leaf.yaml.example  /etc/batt2gen24/bms_config_leaf.yaml
sudo cp /opt/batteryemulator/bms_config_tesla_model3.yaml.example /etc/batt2gen24/bms_config_tesla_model3.yaml
sudo cp /opt/batteryemulator/config.yaml.example /etc/batt2gen24/config.yaml

sudo ln -s /opt/batteryemulator/www/stats.html      /var/www/html/stats.html

sudo ln -s /opt/batteryemulator/systemd/batt2gen24_bms_handler.service      /etc/systemd/system/batt2gen24_bms_handler.service
sudo ln -s /opt/batteryemulator/systemd/batt2gen24_fake_meter.service       /etc/systemd/system/batt2gen24_fake_meter.service
sudo ln -s /opt/batteryemulator/systemd/batt2gen24_logic_handler.service    /etc/systemd/system/batt2gen24_logic_handler.service
sudo ln -s /opt/batteryemulator/systemd/batt2gen24_external_handler.service /etc/systemd/system/batt2gen24_external_handler.service
sudo ln -s /opt/batteryemulator/systemd/batt2gen24_inverter_handler.service /etc/systemd/system/batt2gen24_inverter_handler.service
sudo ln -s /opt/batteryemulator/systemd/batt2gen24_stats_collector.service  /etc/systemd/system/batt2gen24_stats_collector.service

sudo systemctl daemon-reload
sudo systemctl enable batt2gen24_bms_handler
sudo systemctl enable batt2gen24_inverter_handler
sudo systemctl enable batt2gen24_logic_handler
sudo systemctl enable batt2gen24_stats_collector
sudo systemctl enable batt2gen24_external_handler
sudo systemctl start batt2gen24_bms_handler
sudo systemctl start batt2gen24_inverter_handler
sudo systemctl start batt2gen24_logic_handler
sudo systemctl start batt2gen24_stats_collector
sudo systemctl start batt2gen24_external_handler

echo If you use a RS485 Head please edit the /boot/config.txt and reboot your System:
echo # RS485 CAN Hat
echo dtparam=spi=on
echo dtoverlay=mcp2515-can0,oscillator=12000000,interrupt=25,spimaxfrequency=2000000
echo enable_uart=1

echo .
echo For Autostart the CAN communication add the following to the /etc/network/inerfaces
echo auto can0
echo  iface can0 inet Manual
echo  pre-up /sbin/ip link set can0 type can Bitrate 500000
echo  up /sbin/ifconfig can0 up
echo  down /sbin/ifconfig can0 down

