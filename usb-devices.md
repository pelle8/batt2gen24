```
nano /etc/udev/rules.d/20-usb-serial.rules (create a new file if it does not exist)
KERNEL=="ttyUSB[0-9]*", SUBSYSTEM=="tty", KERNELS=="2-1:6:1.0", SYMLINK+="ttyUSBmppt01"
KERNEL=="ttyUSB[0-9]*", SUBSYSTEM=="tty", KERNELS=="2-1:7:1.0", SYMLINK+="ttyUSBmppt02"
KERNEL=="ttyUSB[0-9]*", SUBSYSTEM=="tty", KERNELS=="2-1.1:1.0", SYMLINK+="ttyUSBinv01"
KERNEL=="ttyUSB[0-9]*", SUBSYSTEM=="tty", KERNELS=="2-1.2:1.0", SYMLINK+="ttyUSBinv02"
In my instance the "2-1" is the hardware and the ".2:1.0" is the port on the hardware, which is obtained by looking at a combination of :
dmesg | grep tty
lsusb
ls /sys/bus/usb/devices/
udevadm info –attribute-walk –path=/dev/ttyUSB0
This creates symlinks to each port, which you can then use as the consistent mapping.
```
