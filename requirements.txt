jinja2==3.1.2
markupsafe==2.1.3
msgpack==1.0.5; platform_system != "Windows"
packaging==23.1
paho-mqtt==1.6.1
pymodbus==3.3.2
pyserial==3.5
python-can==4.2.2
pyyaml==6.0
typing-extensions==4.6.3
wrapt==1.15.0
psutil==5.9.8