#!/usr/bin/env python
#
# 2023 Per Carlen
#
# Publishes register-changes to inverter-handler
#  test sub with: $ mosquitto_sub -h localhost -t bms/stat -u "batt2gen24" -P "batt2gen24_pass"
# Subscribes on bms data and register-changes from inverter
#
#

import os
import time
from datetime import datetime
import asyncio
import json
import yaml
import re
import paho.mqtt.client as paho
from typing import List,Any, Dict
import requests
import socket

# global vars
bms_type = ""
bms_update_interval = 60
inverter_type = ""
inverter_uri = ""
last_date_energy_poll = datetime.today().strftime('%Y%m%d')
yesterday_e_tot_produced = 0
yesterday_e_tot_consumed = 0


class StatsHandler:
  def __init__(self):
    self.config = self.read_config_file('config.yaml')
    #self.thread: ModuleThread = ModuleThread('bms_' + self.config['bms_type'], self.run)
    #self.thread.start_thread()

  def run(self):
    while self.thread.running:
      #print("Loading module for BMS: ",self.config['bms_type'])
      #bms = importlib.import_module('bms_' + self.config['bms_type'], package=None)
      #bms_comms = importlib.import_module('bms_comms_' + self.config['bms_type'], package=None)
      #bms.run_updating_server(self,bms_comms)
      time.sleep(1)

  @staticmethod
  def read_config_file(filename: str) -> Dict:

    config = ""
    filename = "config.yaml"
    path = "/etc/batt2gen24/"
    if os.path.isfile(path+filename):
      filename = path + filename
    print("Reading from:",filename)
    try:
      with open(filename,'r') as file:
        config = yaml.safe_load(file)
    except:
      print("ERROR: No config file - ",filename)
      exit(1)
    return config

  def loop(self):
    try:
      while True:
        time.sleep(2)
    except KeyboardInterrupt:
      print("Done!")
      pass

def read_energy_file():
    global yesterday_e_tot_produced
    global yesterday_e_tot_consumed

    yesterday_e_tot_produced = 0
    yesterday_e_tot_consumed = 0
    filename = "/var/www/html/data/energy.json"
    #print("reading from:",filename)
    try:
      with open(filename,'r') as file:
        config_json_str = json.dumps(yaml.safe_load(file))
        config_json = json.loads(config_json_str)
      if 'last_date_energy_poll' in config_json:
        last_date_energy_poll = config_json['last_date_energy_poll']
      if 'yesterday_e_tot_produced' in config_json:
        yesterday_e_tot_produced = config_json['yesterday_e_tot_produced']
      if 'yesterday_e_tot_consumed' in config_json:
        yesterday_e_tot_consumed = config_json['yesterday_e_tot_consumed']
    except:
      pass

    return

def get_energy_from_inverter():
  global inverter_uri
  global last_date_energy_poll
  global yesterday_e_tot_produced
  global yesterday_e_tot_consumed

  #print("reading from inverter")
  date_now = datetime.today().strftime('%Y%m%d')
  try:
    response = requests.get(inverter_uri)
    json_data = json.loads(response.text)
  except:
    json_data = ""
    pass
  if 'Body' in json_data:
    e_tot_produced = -1
    e_tot_consumed = -1
    channels = json_data['Body']['Data']['393216']['channels']
    if 'BAT_ENERGYACTIVE_ACTIVEDISCHARGE_SUM_01_U64' in channels:
      e_tot_produced = int(channels['BAT_ENERGYACTIVE_ACTIVEDISCHARGE_SUM_01_U64'] / 3600)
    if 'BAT_ENERGYACTIVE_ACTIVECHARGE_SUM_01_U64' in channels:
      e_tot_consumed = int(channels['BAT_ENERGYACTIVE_ACTIVECHARGE_SUM_01_U64'] / 3600)
    if e_tot_consumed > -1 and e_tot_produced > -1:
      if last_date_energy_poll != date_now:
        yesterday_e_tot_produced = e_tot_produced
        yesterday_e_tot_consumed = e_tot_consumed
      energy_data = { 'date_now':date_now, 'e_tot_produced': e_tot_produced, 'e_tot_consumed': e_tot_consumed, 'yesterday_e_tot_produced': yesterday_e_tot_produced , 'yesterday_e_tot_consumed': yesterday_e_tot_consumed}     
      f = open("/var/www/html/data/energy.json", "w")
      f.write(json.dumps(energy_data))
      f.close()
      last_date_energy_poll = datetime.today().strftime('%Y%m%d')


def mqtt_on_publish(mqtt_client,userdata,result): 
    #print("data published:",result)
    pass

def extract_minmax_temp_from_cellstats(bms_cellstat_list):
    temp = bms_cellstat_list
    temp_min = 99
    temp_max = -99
    for json_dict in temp:
      if 'cellstat_cell_temp' in json_dict:
        cell_temp = json_dict['cellstat_cell_temp']
        if cell_temp > temp_max:
          temp_max = cell_temp
        if cell_temp < temp_min:
          temp_min = cell_temp
    if temp_min <99 and temp_max > -99:
      ret_dict = { 'logic_cell_temp_min': temp_min, 'logic_cell_temp_max': temp_max }
      return ret_dict
    else:
      return False

def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
    #print(" Received message " + str(message.payload)
    #    + " on topic '" + message.topic
    #    + "' with QoS " + str(message.qos))
    global bms_specific_dict
    global bms_static_dict
    global bms_stat_dict
    global bms_cellstat_list
    global bms_target_dict
    global inverter_data_dict

    json_data = json.loads(message.payload)
    if message.topic == "bms/static":
      bms_static_dict = json_data
    if message.topic == "bms/stat":
      bms_stat_dict = json_data
    if message.topic == "bms/cellstat":
      bms_cellstat_list = json_data
    if message.topic == "bms/target":
      bms_target_dict = json_data
    if message.topic == "bms/specific":
      bms_specific_dict = json_data
    if message.topic == "inverter/data":
      inverter_data_dict = json_data

    bms_data = {}
    bms_data.update(bms_static_dict)
    bms_data.update(bms_stat_dict)
    bms_data.update(bms_target_dict)
    bms_data.update(bms_specific_dict)
    try:
      f = open("/var/www/html/data/bms.json", "w")
      f.write(json.dumps(bms_data))
      f.close()
    except Exception:
      pass
 


def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    #print(" Received message " + str(client)
    #    + "' with QoS " + str(granted_qos))
    pass


def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
      print("MQTT connection problem")
      client.connected_flag=False
    else:
      print("MQTT client connected:" + str(client))
      client.connected_flag=True
      client.subscribe("bms/stat",2)
      client.subscribe("bms/cellstat",2)
      client.subscribe("bms/static",2)
      client.subscribe("bms/specific",2)
      client.subscribe("bms/target",2)
      client.subscribe("inverter/data",2)

async def run_server() -> None:
    global mqtt_client2
    global bms_type
    global inverter_uri
    global mqtt_client_smarthome
    global smarthome

    global bms_static_dict
    global bms_stat_dict 
    global bms_target_dict 
    global bms_specific_dict 
    global bms_cellstat_list
    global inverter_data_dict

    inverter_data_dict = {}
    bms_static_dict = {}
    bms_stat_dict = {}
    bms_cellstat_list = {}
    bms_target_dict = {}
    bms_specific_dict = {}
    smarthome = False

    stats_collector = StatsHandler()
    config = stats_collector.config

    print("\nStarting stats collector")
    if config:
      bms_type = config['bms_type']
      mqtt_broker = config['mqtt_broker']
      mqtt_port = config['mqtt_port']
      mqtt_username = config['mqtt_username']
      mqtt_password = config['mqtt_password']
      if 'inverter_uri' in config:
        inverter_uri = config['inverter_uri']
    else:
      exit(1)
    print("Read old energy stats file")
    read_energy_file()
    # this mqtt client will subscribe
    mqtt_client2 = paho.Client(client_id="stats_collector",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client2.loop_start()

    try:
      mqtt_smarthome_broker = config['mqtt_smarthome_broker']
      mqtt_smarthome_port = config['mqtt_smarthome_port']
      mqtt_smarthome_username = config['mqtt_smarthome_username']
      mqtt_smarthome_password = config['mqtt_smarthome_password']
      smarthome = True
    except KeyError:
      print("no Smarthome MQTT defined")
    mqtt_client_smarthome = ""
    if smarthome:
        mqtt_client_smarthome = paho.Client(client_id=socket.gethostname(), transport="tcp",
                                            protocol=paho.MQTTv311,
                                            clean_session=True)
        mqtt_client_smarthome.on_publish = mqtt_on_publish
        mqtt_client_smarthome.on_connect = mqtt_on_connect
        mqtt_client_smarthome.username_pw_set(mqtt_smarthome_username, mqtt_smarthome_password)
        mqtt_client_smarthome.connect(mqtt_smarthome_broker, mqtt_smarthome_port, keepalive=60)
        mqtt_client_smarthome.loop_start()

    try:
      seconds = 0
      while True:
        seconds += 1
        if seconds > 59:
          get_energy_from_inverter()
          seconds = 0
        if smarthome:
          data_to_public = {"bms/static": bms_static_dict,
                            "bms/stat": bms_stat_dict,
                            "bms/target": bms_target_dict,
                            "bms/specific": bms_specific_dict,
                            "bms/cellstat": bms_cellstat_list,
                            "inverter/data": inverter_data_dict}
          json_string = json.dumps(
              data_to_public,
              separators=(',', ':'))
          mqtt_client_smarthome.publish('tele/batt2gen24_'+socket.gethostname()+'/STATE', json_string)
        
        await asyncio.sleep(1)

    except KeyboardInterrupt:
        # Wait for last message to arrive
        await reader.get_message()
        global debug
        print("Done!")

        # Clean-up
        mqtt_client2.disconnect()
        pass  # exit normally


if __name__ == '__main__':
    asyncio.run(run_server())
