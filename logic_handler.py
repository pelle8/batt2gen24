#!/usr/bin/env python

# 2023 Per Carlen
#
# This is the logic between a bms and an inverter
#
# Publishes register-changes to inverter-handler
#  test sub with: $ mosquitto_sub -h localhost -t bms/stat -u "batt2gen24" -P "batt2gen24_pass"
# Subscribes on bms data and register-changes from inverter
#
#

import hashlib
import os
import time
import copy
import asyncio
import json
import yaml
import re
import paho.mqtt.client as paho
from typing import List,Dict
import importlib


# Logic 

class LogicHandler:
  def __init__(self):
    self.config = self.read_config_file('config.yaml')

  @staticmethod
  def read_config_file(filename: str) -> Dict:
    config = ""
    filename = "config.yaml"
    path = "/etc/batt2gen24/"
    if os.path.isfile(path+filename):
      filename = path + filename
    print("Reading from:",filename)
    try:
      with open(filename,'r') as file:
        config = yaml.safe_load(file)
    except:
      print("ERROR: No config file - ",filename)
      exit(1)
    return config

# global vars
bms_type = ""
bms_update_interval = 60

inverter_data = {}

bms_static_dict = {}
bms_stat_dict = {}
bms_cellstat_list = {}
bms_target_dict = {}
bms_specific_dict = {}
logic_config ={'logic_debug' : 0 }


def init_inverter_data(inverter,inverter_type):
  global inverter_data
  global bms_static_dict

  inverter_data_string = inverter.init_inverter_data(bms_static_dict) 
  if inverter_data_string:
    inverter_data = json.loads(inverter_data_string)
    inverter_data['bms_fault'] = True
    inverter_data['bms_online'] = True


def read_config_file():

    config_json = ""
    filename = "config.yaml"
    path = "/etc/batt2gen24/"
    if os.path.isfile(path+filename):
        filename = path + filename
    print("reading from:",filename)
    try:
      with open(filename,'r') as file:
        config_json = json.dumps(yaml.safe_load(file))
    except:
      print("ERROR: No config file - ",filename)
      exit(1)

    return config_json


def interval_action(mqtt_client,counter):
    global logic_config
    global bms_specific_dict
    global inverter_data


    # TODO: check "packCtrCloseBlocked"...if that is 1, you might need to open up the penthouse and disconnect the bms for 5 mins
    if bms_type == "tesla_model3":       # every 30ms
      if inverter_data['bms_fault'] == False:
        json_string = "close_contactor_interval"
        topic = "bms/control"
        ret = mqtt_client.publish(topic,json_string) 
      # or closed or...not open
      if inverter_data['bms_fault'] == True:
        if 'specific_contactor' in bms_specific_dict and 'specific_packContPositiveState' in bms_specific_dict and 'specific_packContNegativeState' in bms_specific_dict:
          if re.search("CLOSED",bms_specific_dict['specific_contactor']) or \
          re.search("ECONOMIZED",bms_specific_dict['specific_packContPositiveState']) or \
          re.search("ECONOMIZED",bms_specific_dict['specific_packContNegativeState']):
            json_string = "open_contactors"
            topic = "bms/control"
            ret = mqtt_client.publish(topic,json_string) 
      return

    if bms_type == "leaf":       # every 10ms
      if counter % 100 == 0: # every 1secs
        json_string = "get_various"
        topic = "bms/control"
        ret = mqtt_client.publish(topic,json_string) 

      if counter % 1000 == 50: # every 10secs
        json_string = "get_cell_voltage"
        topic = "bms/control"
        ret = mqtt_client.publish(topic,json_string) 
      if counter % 1000 == 150: # every 10secs
        json_string = "get_cell_temp"
        topic = "bms/control"
        ret = mqtt_client.publish(topic,json_string) 
      if counter % 1000 == 250: # every 10secs
        json_string = "get_shunts"
        topic = "bms/control"
        ret = mqtt_client.publish(topic,json_string) 

      json_string = "interval_10ms"
      topic = "bms/control"
      ret = mqtt_client.publish(topic,json_string) 
      if counter % 10 == 0:
        json_string = "interval_100ms"
        topic = "bms/control"
        ret = mqtt_client.publish(topic,json_string) 




def mqtt_on_publish(mqtt_client,userdata,result): 
    global logic_config
    pass


def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
    #print(" Received message " + str(message.payload)
    #    + " on topic '" + message.topic
    #    + "' with QoS " + str(message.qos))
    global bms_specific_dict
    global bms_static_dict
    global bms_stat_dict
    global bms_cellstat_list
    global bms_target_dict  
    global inverter_data
    global bms_type
    global logic_config
    global have_all_static_vars
    global inverter
    global last_update_from_inverter
    global last_update_from_bms

    #print("Got MQTT:",message.payload)
    json_data = json.loads(message.payload)
    if message.topic == "inverter/writes":
      last_update_from_inverter = int(json_data['last_update'])
    if message.topic == "bms/static":
      bms_static_dict = bms_static_dict | json_data
    if message.topic == "bms/stat":
      bms_stat_dict = bms_stat_dict | json_data
      last_update_from_bms = int(json_data['last_updated'])
    if message.topic == "bms/cellstat":
      bms_cellstat_list = json_data
    if message.topic == "bms/target":
      bms_target_dict = bms_target_dict | json_data
    if message.topic == "bms/specific":
      bms_specific_dict = bms_specific_dict | json_data
    if message.topic == "external/status":
      inverter_data = inverter_data | json_data

    bms_data = {}
    bms_data.update(bms_static_dict)
    bms_data.update(bms_stat_dict)
    bms_data.update(bms_target_dict)
    bms_data.update(bms_specific_dict)

    #rc = extract_minmax_temp_from_cellstats(bms_cellstat_list)
    #if type(rc) is dict:
    #  bms_data.update(rc)

    old_inverter_data = copy.deepcopy(inverter_data)
    # we will get registers back that we will publish
    temp = inverter.got_message(message.topic,message.payload,bms_data,inverter_data,logic_config,bms_type,mqtt_client)
#      if type(temp) is dict or type(temp) is list:
    if type(temp) is dict:
      inverter_data = temp
      old_json_str = json.dumps(old_inverter_data)
      new_json_str = json.dumps(inverter_data)
      new_json = re.sub(r"\"last_updated.*,", "", new_json_str)
      old_json = re.sub(r"\"last_updated.*,", "", old_json_str)
      sha_1_new = hashlib.sha1(new_json.encode('ascii')).hexdigest()
      sha_1_old = hashlib.sha1(old_json.encode('ascii')).hexdigest()
      if sha_1_new != sha_1_old:
        if logic_config['logic_debug']:  print("new data to inverter...")
        publish_inverter_data(mqtt_client)


def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    pass


def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
      print("MQTT connection problem")
      client.connected_flag=False
    else:
      print("MQTT client connected:" + str(client))
      client.connected_flag=True
      client.subscribe("bms/stat",2)
      client.subscribe("bms/cellstat",2)
      client.subscribe("bms/static",2)
      client.subscribe("bms/target",2)
      client.subscribe("bms/specific",2)
      client.subscribe("inverter/writes",2)
      client.subscribe("external/status",2)

def publish(client,pub_var,topic):
    json_string = json.dumps(pub_var)
    ret = client.publish(topic,json_string) 

def publish_inverter_data(mqtt_client):
    global inverter_data
    global logic_config
    global have_all_static_vars

    if have_all_static_vars:
      if logic_config['logic_debug']: print("Publishing inverter/data")
      if logic_config['logic_debug']: print(inverter_data)
#      json_string = json.dumps( inverter_data )
#      print(json_string)
      publish(mqtt_client,inverter_data,"inverter/data")

def check_bms_faults(bms_type,status_dict,mqtt_client1,bms_specific):
  global bms_config
  global inverter_data
  global logic_config

  must_have_keys = ['cell_temp_min','cell_temp_max','cell_voltage_min','cell_voltage_max']
  if all(k in inverter_data for k in must_have_keys): # Check that all values are within limits
    if inverter_data['cell_temp_min'] < bms_config['static_min_cell_temperature'] or \
     inverter_data['cell_temp_max'] > bms_config['static_max_cell_temperature'] or \
     (inverter_data['cell_voltage_min'] < bms_config['static_min_cell_voltage'] and not 'ignore_minimum_voltage' in logic_config) or \
     inverter_data['cell_voltage_max'] > bms_config['static_max_cell_voltage']:
      status_dict['bms_fault'] = True
      print("ERROR: Cell values are out of bounds!")
    else:
      status_dict['bms_fault'] = False
  else:
    print("ERROR: Can't find all necessary bms data!")
    status_dict['bms_fault'] = True

  if 'specific_low_voltage' in bms_specific and 'static_min_low_voltage' in bms_config:
    if float(bms_specific['specific_low_voltage']) < float(bms_config['static_min_low_voltage']):
      print("ERROR: Low voltage(12V) is too low!")
      status_dict['bms_fault'] = True

  if 'specific_LB_Relay_Cut_Request' in bms_specific:
    if int(bms_specific['specific_LB_Relay_Cut_Request']) > 0:
      status_dict['bms_fault'] = True

  if status_dict['bms_online'] == False:
    print("ERROR: BMS is offline!")
    status_dict['bms_fault'] = True

  if 'test_bms_fault' in bms_specific:
    if bms_specific['test_bms_fault'] == 1:
      print("ERROR: Testing bms fault!")
      status_dict['bms_fault'] = True

  if status_dict['bms_fault']: # OK, we got a fault here, let's open up the contactors
    if bms_type == "leaf":
      json_dict = {'type':'contactors','command':'open'}  
      publish(mqtt_client1,json_dict,"external/control")
    if bms_type == "batrium":
      json_dict = {'type':'relay', 'id':0, 'state':'on'}  
      publish(mqtt_client1,json_dict,"external/control")
  else: # Everything is ok, close the contactors
    if bms_type == "leaf":
      json_dict = {'type':'contactors','command':'close'}  
      publish(mqtt_client1,json_dict,"external/control")
    if bms_type == "batrium":
      json_dict = {'type':'relay', 'id':0, 'state':'on'}  
      publish(mqtt_client1,json_dict,"external/control")

  if status_dict['bms_fault']:
    print("ERROR: BMS Fault!!!")
  return status_dict['bms_fault']


def read_bms_config_file(config):

    global bms_type

    config_json = ""
    filename = "bms_config_" + config['bms_type'] + ".yaml"
    path = "/etc/batt2gen24/"
    if os.path.isfile(path+filename):
        filename = path + filename
    print("Reading from:",filename)
    try:
      with open(filename,'r') as file:
        config_json = yaml.safe_load(file)
    except:
      pass

    must_have_keys = ['static_number_of_cells','static_min_cell_voltage','static_max_cell_voltage','static_min_cell_temperature','static_max_cell_temperature']
    if all(k in config_json for k in must_have_keys):
      return config_json
    else:
      print("Missing configuration items in",filename,"need:",must_have_keys)
      exit(1)


async def main() -> None:
    global mqtt_client1
    global mqtt_client2

    global bms_type
    global inverter_data
    global bms_specific_dict

    global logic_config
    global bms_config
    global have_all_static_vars
    global inverter
    global last_update_from_inverter
    global last_update_from_bms

    logic_handler = LogicHandler()
    logic_config = logic_handler.config
    bms_config = read_bms_config_file(logic_config)
    bms_type = logic_config['bms_type']
    mqtt_broker = logic_config['mqtt_broker']
    mqtt_port = logic_config['mqtt_port']
    mqtt_username = logic_config['mqtt_username']
    mqtt_password = logic_config['mqtt_password']

    print("\nStarting logic for " + logic_config['inverter_type'] + " and " + bms_type)

    print("Loading module for inverter: ",logic_config['inverter_type'])
    inverter = importlib.import_module('logic', package=None)

    # this mqtt client will publish
    mqtt_client1 = paho.Client(client_id="logic_pub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client1.on_publish = mqtt_on_publish
    mqtt_client1.on_connect = mqtt_on_connect
    mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client1.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client1.loop_start()

    # this mqtt client will subscribe
    mqtt_client2 = paho.Client(client_id="logic_sub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client2.loop_start()

    temp = {}

    have_all_static_vars = False
    while not have_all_static_vars:
      must_have_keys = ['static_nominal_capacity','static_max_voltage','static_min_voltage','static_max_power']
      if all(k in bms_static_dict for k in must_have_keys):
        init_inverter_data(inverter,logic_config['inverter_type'])
        have_all_static_vars = True
      await asyncio.sleep(1)

    print("\nGot all static data")

    interval_counter = 0
    seconds = 0
    milliseconds = 0 # counts ms

    have_all_bms_data = False
    while not have_all_bms_data:
      # Need to start interval tasks to get data from leaf bms
      if seconds > logic_config['inverter_update_interval']:
        publish_inverter_data(mqtt_client1)
        seconds = 0
      await asyncio.sleep(0.010)
      milliseconds += 10
      seconds += 0.010
      if milliseconds == logic_config['logic_interval']:
        interval_action(mqtt_client1,interval_counter)
        milliseconds = 0
      interval_counter += 1
      if interval_counter > 10000:
        interval_counter = 0

      must_have_keys = ['cell_temp_min','cell_temp_max','cell_voltage_min','cell_voltage_max']
      if all(k in inverter_data for k in must_have_keys):
        have_all_bms_data = True

      await asyncio.sleep(0.010)

    print("\nGot all data, starting up...")
    last_update_from_inverter = 0
    last_update_from_bms = int(time.time())
    inverter_data['bms_type'] = bms_type
    inverter_data['inverter_type'] = logic_config['inverter_type']
    try:
        while True:
          time_now = int(time.time())

          if time_now - last_update_from_inverter > logic_config['inverter_timeout']:
            inverter_data['inverter_online'] = False
          else:
            inverter_data['inverter_online'] = True

          if time_now - last_update_from_bms > logic_config['bms_timeout']:
            inverter_data['bms_online'] = False
          else:
            inverter_data['bms_online'] = True

          if interval_counter % 100 == 0: # Every second
            inverter_data['bms_fault'] = check_bms_faults(bms_type,inverter_data,mqtt_client1,bms_specific_dict)
          if seconds > logic_config['inverter_update_interval']:
            publish_inverter_data(mqtt_client1)
            seconds = 0
          await asyncio.sleep(0.010)
          milliseconds += 10
          seconds += 0.010
          if milliseconds == logic_config['logic_interval']:
            interval_action(mqtt_client1,interval_counter)
            milliseconds = 0
          interval_counter += 1
          if interval_counter > 10000:
            interval_counter = 0

    except KeyboardInterrupt:
        # Wait for last message to arrive
        await reader.get_message()
        print("Done!")

        # Clean-up
        notifier.stop()
        mqtt_client1.disconnect()
        mqtt_client2.disconnect()
        pass  # exit normally


if __name__ == "__main__":
    asyncio.run(main())
