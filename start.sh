#/usr/sbin/ip link set can0 type can restart-ms 100
#/usr/sbin/ip link set can1 type can restart-ms 100
ifconfig can0 txqueuelen 100
ifconfig can1 txqueuelen 100 || :
ip link set can0 type can bitrate 500000
ip link set can1 type can bitrate 500000 || :
ip link set can0 up
ip link set can1 up || :
