#!/usr/bin/env python

# 2023 Per Carlén
#
# This is a decoder for Leaf BMS.
# This module decodes a can-message, and returns a json-string
# Almost everything related to can messages and their content comes from Dala, thanks!


import can
import json
import struct
import re
import time

battery_request_idx = 0
main_cell_voltages = {}
ignore_7bb = True
group_7bb = 0
can_storage = {}

def Temp_fromRAW_to_F(i):
  if i == 1021:
    return 1.0
  elif i >= 589:
    return 162.0 - (i * 0.181)
  elif i >= 569:
    return 57.2 + ((579 - i) * 0.18)
  elif i >= 558:
    return 60.8 + ((558 - i) * 0.16363636363636364)
  elif i >= 548:
    return 62.6 + ((548 - i) * 0.18)
  elif i >= 537:
    return 64.4 + ((537 - i) * 0.16363636363636364)
  elif i >= 447:
    return 66.2 + ((527 - i) * 0.18)
  elif i >= 438:
    return 82.4 + ((438 - i) * 0.2)
  elif i >= 428:
    return 84.2 + ((428 - i) * 0.18)
  elif i >= 365:
    return 86.0 + ((419 - i) * 0.2)
  elif i >= 357:
    return 98.6 + ((357 - i) * 0.225)
  elif i >= 348:
    return 100.4 + ((348 - i) * 0.2)
  elif i >= 316:
    return 102.2 + ((340 - i) * 0.225)
  return 109.4 + ((309 - i) * 0.2571428571428572);


def decode(msg: can.Message,bms_debug) -> None:
    global main_cell_voltages
    global battery_request_idx
    global ignore_7bb
    global group_7bb
    global can_storage

    time_now = time.time()
    id = hex(msg.arbitration_id)
    data = msg.data

    if id == "0x55b":
      LB_TEMP = (data[0] << 2 | data[1] >> 6)
      soc = 0
      if (LB_TEMP != 0x3ff) :
        soc = LB_TEMP/10
      #if soc > 0:
        #print("soc:",soc,data)
        #json_string = json.dumps({'last_update' : time_now, 'stat_soc': soc }, separators=(',', ':'))
        #return json_string
      return False

    if id == "0x5bc":
      #print(data)
      LB_Average_Battery_Temperature = ((data[5] & 0x10) >> 4)

      WH_PER_GID = 77
      LB_MAX = ((data[5] & 0x10) >> 4)
      if (LB_MAX):
        LB_Max_GIDS = (data[0] << 2) | ((data[1] & 0xC0) >> 6)
        #//Max gids active, do nothing
        #//Only the 30/40/62kWh packs have this mux
      else:
        # //Normal current GIDS value is transmitted
        LB_GIDS = (data[0] << 2) | ((data[1] & 0xC0) >> 6)
        LB_Wh_Remaining = (LB_GIDS * WH_PER_GID)

      LB_TEMP = (data[4] >> 1)
      if (LB_TEMP != 0):
        LB_StateOfHealth = LB_TEMP # Collect state of health from battery

      soc = 100 * LB_Wh_Remaining / ( LB_StateOfHealth/100 * 24000 )
      #print("SoH/Wh_remain,soc:",LB_StateOfHealth,LB_Wh_Remaining,soc)
      json_string = json.dumps({'last_update' : time_now, 'stat_soh': LB_StateOfHealth }, separators=(',', ':'))
      return json_string


    if id == "0x5c0": # Always reads temp as 0 degrees, better read them instead as with cell voltages
      mux = data[0] >> 6
      temp = (data[2] << 1) * 2 - 40 
      #print("mux",mux,temp)
      LB_HeatExist = (data[4] & 0x01)
      LB_Heating_Stop = ((data[0] & 0x10) >> 4)
      LB_Heating_Start = ((data[0] & 0x20) >> 5)
      Batt_Heater_Mail_Send_Request = (data[1] & 0x01)
      json_string = json.dumps({'last_update' : time_now, 'specific_LB_HeatExist': LB_HeatExist, 'specific_LB_HeatExist': LB_Heating_Stop, 'specific_LB_Heating_Stop': LB_Heating_Start, 'specific_Batt_Heater_Mail_Send_Request': Batt_Heater_Mail_Send_Request }, separators=(',', ':'))
      return json_string

    if id == "0x1dc": # Target charge/discharge (this is limit values)
      d1 = data[0] << 2 
      d2 = (data[1] & 192) >> 6
      target_discharge_power = int(d1 + d2) / 4 
      d1 = (data[1] & 63) << 4 
      d2 = (data[2] & 240) >> 4
      target_charge_power = int(d1 + d2) / 4 
      #print("C,D:",target_charge_power,target_discharge_power)
      return False

    if id == "0x1db":

      batt_amps = (data[0] << 3) | (data[1] & 0xe0) >> 5
      if batt_amps & 0x0400:
        batt_amps |= 0xf800
        batt_amps = - (65535-batt_amps)
      batt_voltage = ((data[2] << 2) | (data[3] & 0xc0) >> 6) / 2
      LB_Relay_Cut_Request = ((data[1] & 0x18) >> 3)
      LB_Failsafe_Status = (data[1] & 0x07)
      LB_MainRelayOn_flag = ((data[3] & 0x20) >> 5)
      LB_Full_CHARGE_flag = ((data[3] & 0x10) >> 4)
      LB_Interlock = ((data[3] & 0x08) >> 3)
      #print("U/I:",batt_voltage,batt_amps)
      LB_Failsafe_Status_text = ""
      if LB_Failsafe_Status == 0:
        LB_Failsafe_Status_text = "OK"
      elif LB_Failsafe_Status & 1:
        LB_Failsafe_Status_text += "EV system erro"
      elif LB_Failsafe_Status & 2:
        LB_Failsafe_Status_text += "Charge disabled"
      elif LB_Failsafe_Status & 4:
        LB_Failsafe_Status_text += "Turtle mode"


      if LB_Failsafe_Status != 0:
        if bms_debug: print("LB_Relay_Cut_Request,LB_Failsafe_Status,LB_MainRelayOn_flag,LB_Full_CHARGE_flag,LB_Interlock:",LB_Relay_Cut_Request,LB_Failsafe_Status_text,LB_MainRelayOn_flag,LB_Full_CHARGE_flag,LB_Interlock)
      try:
          json_string = json.dumps({'last_update' : time_now, 'specific_LB_Failsafe_Status': LB_Failsafe_Status_text, 'specific_LB_Interlock': LB_Interlock, 'specific_LB_Relay_Cut_Request': LB_Relay_Cut_Request}, separators=(',', ':'))
          return json_string
      except:
        pass

#      batt_power = batt_voltage * batt_amps
#      json_string = json.dumps({'last_update' : time_now, 'stat_batt_voltage': batt_voltage, 'stat_batt_current': batt_amps, 'stat_batt_power': batt_power }, separators=(',', ':'))
      return False

    if id == "0x7bb": # Here comes a lot of stuff...
      if data[0] == 16:
        ignore_7bb = True
        group_7bb = data[3]
        if group_7bb == 1 or group_7bb == 2 or group_7bb == 4 or group_7bb == 6: ignore_7bb = False

      if not ignore_7bb:

        if group_7bb == 1: # soc, current, voltage etc - much more accurate than 0x1db - TODO: replace
          if data[0] == 16:
            can_storage = {}
            can_storage.update( {data[0]:data[0:8] })
            return 'next_line_request'    

          if data[0] >32 and data[0] < 40:
            can_storage.update( {data[0]:data[0:8] })

          #if data[0] == 0x27 and data[6] ==0xFF : # last frame
          if data[0] == 0x25: # last frame
            #print("cs:",can_storage)
            hx = (( can_storage[36][2] << 8) | can_storage[36][3]) / 102.4
            soc = ( can_storage[36][5] << 16 | (can_storage[36][6] << 8) | can_storage[36][7] ) /10000
            ahr = ( can_storage[37][2] << 16 | (( can_storage[37][3] << 8) | can_storage[37][4]))/10000
            batt_u = (( can_storage[35][1] << 8) | can_storage[35][2]) / 100
            batt_i = can_storage[33][5] << 8 | can_storage[33][6]
            if batt_i & 0x8000 == 0x8000 :
              batt_i = (batt_i | -0x10000 ) / 1024
            else:
              batt_i = batt_i / 1024
            batt_i_1 = ( can_storage[16][4] << 24) | ( can_storage[16][5] << 16 | (( can_storage[16][6] << 8) | can_storage[16][7]))
            if batt_i_1 & 0x8000000 == 0x8000000 :
              batt_i_1 = (batt_i_1 | -0x100000000 ) / 1024
            else:
              batt_i_1 = batt_i_1 / 1024
            batt_i_2 = ( can_storage[33][3] << 24) | ( can_storage[33][4] << 16 | (( can_storage[33][5] << 8) | can_storage[33][6]))
            if batt_i_2 & 0x8000000 == 0x8000000 :
              batt_i_2 = (batt_i_2 | -0x100000000 ) / 1024
            else:
              batt_i_2 = batt_i_2 / 1024
            
            #print("soc/hx/ahr/batt_i/batt_i_1/batt_i_2/batt_u:",soc,hx,ahr,batt_i,batt_i_1,batt_i_2,batt_u)
            batt_power = batt_u * batt_i_2
            nominal_full = ahr * 360  / 1000 # nominal voltage = 360V
            json_dict = {'last_update' : time_now, 'stat_soc': soc,'stat_batt_voltage': batt_u, 'stat_batt_current': batt_i_2, 'stat_batt_power': batt_power, 'stat_nominal_full_pack_energy': nominal_full }
            json_string = json.dumps(json_dict, separators=(',', ':'))
            return json_string
          return 'next_line_request'    

        if group_7bb == 2: # Cell voltages
          if data[0] == 16:
            main_cell_voltages = [0] * 100
            battery_request_idx = 0
            main_cell_voltages[battery_request_idx] = data[4] << 8 | data[5]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[6] << 8 | data[7]
            battery_request_idx += 1
            return 'next_line_request'    

          if data[0] == 0x2C and data[6] ==0xFF : # last frame
            json_temp = ""
            cell_min = 5
            cell_max = 0
            for cell_no in range(0,96):
              cell_voltage = main_cell_voltages[cell_no] / 1000
              if cell_voltage >= 0 and cell_voltage < 5:
                if cell_voltage > cell_max: cell_max = cell_voltage
                if cell_voltage < cell_min: cell_min = cell_voltage
                json_temp += json.dumps({'last_update' : time_now, 'cellstat_cell_num': cell_no + 1 , 'cellstat_max_voltage': cell_voltage, 'cellstat_min_voltage': cell_voltage }) + ","
            json_stat = json.dumps({'last_update' : time_now, 'stat_min_cell_u': cell_min, 'stat_max_cell_u': cell_max}, separators=(',', ':'))
            json_string = "[" + json_temp + json_stat + "]"
            #print("js:",json_string)
            return json_string

          #print("data:",data)
          if data[0] % 2 == 0: # even frames
            main_cell_voltages[battery_request_idx] |= data[1]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[2] << 8 | data[3]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[4] << 8 | data[5]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[6] << 8 | data[7]
            battery_request_idx += 1
          else:  
            main_cell_voltages[battery_request_idx] = data[1] << 8 | data[2]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[3] << 8 | data[4]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[5] << 8 | data[6]
            battery_request_idx += 1
            main_cell_voltages[battery_request_idx] = data[7] << 8

          return 'next_line_request'


        if group_7bb == 4: # temperature 
          if data[0] == 16:
            can_storage = {}
            can_storage.update( {data[0]:data[0:8] })
            return 'next_line_request'    

          if data[0] >32 and data[0] < 35:
            can_storage.update( {data[0]:data[0:8] })

          if data[0] == 0x22 and data[6] ==0xFF : # last frame
            temp_list = [float(0)] * 4
            min_temp = 99
            max_temp = -99
            temp = (can_storage[16][4]) << 8 | (can_storage[16][5])
            temp_list[0] = float( (Temp_fromRAW_to_F(temp)-32) *5 / 9 )
            temp = (can_storage[16][7]) << 8 | (can_storage[33][1])
            temp_list[1] = (Temp_fromRAW_to_F(temp)-32) *5 / 9
            temp = (can_storage[33][3]) << 8 | (can_storage[33][4])
            temp_list[2] = (Temp_fromRAW_to_F(temp)-32) *5 / 9
            temp = (can_storage[33][6]) << 8 | (can_storage[33][7])
            temp_list[3] = (Temp_fromRAW_to_F(temp)-32) *5 / 9
            for t in temp_list:
              if t > -50 and t < 100:
                if t > max_temp: max_temp = t
                if t < min_temp: min_temp = t
            json_temp1 = json.dumps( {'last_update' : time_now, 'cellstat_cell_num': (1), 'cellstat_cell_temp': min_temp, 'stat_min_cell_t': min_temp } )
            json_temp2 = json.dumps( {'last_update' : time_now, 'cellstat_cell_num': (2), 'cellstat_cell_temp': max_temp, 'stat_max_cell_t': max_temp } )
            json_string = "[" + json_temp1 + "," + json_temp2 + "]"
            return json_string

          return 'next_line_request'

        if group_7bb == 6: # balancing - shunts 
          if data[0] == 16:
            can_storage = {}
            can_storage.update( {data[0]:data[0:8] })
            return 'next_line_request'    

          if data[0] >32 and data[0] < 36:
            can_storage.update( {data[0]:data[0:8] })

          if data[0] == 0x23 and data[7] ==0xFF : # last frame
            #print("last frame shunt")
            shunt_data = can_storage[16][4:8] + can_storage[33][0:8] + can_storage[34][0:1]
            shunt_status = [0] * 96
            shunt_bits = [-1.0] * 24
            shunt_order = "8421"
            for bit in range(12):
              shunt_bits[bit*2] = shunt_data[bit] & 0xF
              shunt_bits[(bit*2) + 1] = shunt_data[bit] >> 4
            json_temp = ""
            for i in range(96):
              i3 = shunt_bits[i >> 2]
              j = int(shunt_order[i % 4])
              if (j & i3) != 0 :
                shunt_status[i] = 1
              bypass_value = shunt_status[i] * 50
              #print("shunt:",i,bypass_value)
              json_temp += json.dumps({'last_update' : time_now, 'cellstat_cell_num': i + 1, 'cellstat_cell_balancing': bypass_value}) + ","
            new_json = re.sub(r",$", "", json_temp)
            json_string = "[" + new_json + "]"
            return json_string

          return 'next_line_request'

    return False

def send_command(bus,msg,bms_debug) -> None:
    global counter_10
    global counter_100

    time_now = time.time()
    command = str(msg)

    if bms_debug: print("Time to send:" + command + " to leaf bms")

    if command == "get_cell_voltage": 
      
      LEAF_data = [0x02,0x21,0x02,0x00,0x00,0x00,0x00,0x00]
      msg = can.Message(arbitration_id=0x79b, timestamp=time.time(),
                        data=LEAF_data, is_extended_id=False)
      try:
          bus.send(msg)
          if bms_debug: print("Frame 79B was successful sent.")
      except can.CanError:
          print(msg, "Frame 79B could not be sent.")
          pass

      return True

    if command == "get_cell_temp": 
      
      LEAF_data = [0x02,0x21,0x04,0x00,0x00,0x00,0x00,0x00]
      msg = can.Message(arbitration_id=0x79b, timestamp=time.time(),
                        data=LEAF_data, is_extended_id=False)
      try:
          bus.send(msg)
          if bms_debug: print("Frame 79B was successful sent.")
      except can.CanError:
          print(msg, "Frame 79B could not be sent.")
          pass

      return True

    if command == "next_line_request": 
      LEAF_data = [0x30,0x01,0x00,0xff,0xff,0xff,0xff,0xff]
      msg = can.Message(arbitration_id=0x79b, timestamp=time.time(),
                        data=LEAF_data, is_extended_id=False)
      try:
        bus.send(msg)
        if bms_debug: print("Frame 79B was successful sent.")
      except can.CanError:
        print(msg, "Frame 79B could not be sent.")
        pass
        return False
      return True

    if command == "get_shunts": 
      
      LEAF_data = [0x02,0x21,0x06,0x00,0x00,0x00,0x00,0x00]
      msg = can.Message(arbitration_id=0x79b, timestamp=time.time(),
                        data=LEAF_data, is_extended_id=False)
      try:
          bus.send(msg)
          if bms_debug: print("Frame 79B was successful sent.")
      except can.CanError:
          print(msg, "Frame 79B could not be sent.")
          pass

      return True

    if command == "get_various": 
      
      LEAF_data = [0x02,0x21,0x01,0x00,0x00,0x00,0x00,0x00]
      msg = can.Message(arbitration_id=0x79b, timestamp=time.time(),
                        data=LEAF_data, is_extended_id=False)
      try:
          bus.send(msg)
          if bms_debug: print("Frame 79B was successful sent.")
      except can.CanError:
          print(msg, "Frame 79B could not be sent.")
          pass

      return True




    # rather ugly, init counter here
    try:
      temp = counter_10
    except:
      counter_10 = 0
      pass

    # rather ugly, init counter here
    try:
      temp = counter_100
    except:
      counter_100 = 0
      pass



    if command == "interval_100ms": # stay closed without sending periodically

      LEAF_50B_data = [0x00, 0x00, 0x06, 0xC0, 0x00, 0x00, 0x00]
      LEAF_50C_data = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00]

      # Sending 0x50b message:

      #//When battery requests heating pack status change, ack this
      #if (Batt_Heater_Mail_Send_Request) {
      #  LEAF_50B.data.u8[6] = 0x20;  //Batt_Heater_Mail_Send_OK
      #} else {
      #  LEAF_50B.data.u8[6] = 0x00;  //Batt_Heater_Mail_Send_NG
      #}

      msg = can.Message(arbitration_id=0x50b, timestamp=time.time(),
                        data=LEAF_50B_data, is_extended_id=False)
      try:
          bus.send(msg)
          if bms_debug: print("Frame 50B was successful sent.")
      except can.CanError:
          print(msg, "Frame 50B could not be sent.")


      # Sending 0x50c message:

      if counter_100 % 4 == 0:
        LEAF_50C_data[3] = 0x00
        LEAF_50C_data[4] = 0x5D
        LEAF_50C_data[5] = 0xC8
      if counter_100 % 4 == 1:
        LEAF_50C_data[3] = 0x01
        LEAF_50C_data[4] = 0xB2
        LEAF_50C_data[5] = 0x31
      if counter_100 % 4 == 2:
        LEAF_50C_data[3] = 0x02
        LEAF_50C_data[4] = 0x5D
        LEAF_50C_data[5] = 0x63
      if counter_100 % 4 == 3:
        LEAF_50C_data[3] = 0x03
        LEAF_50C_data[4] = 0xB2
        LEAF_50C_data[5] = 0x9A

      msg = can.Message(arbitration_id=0x50c, timestamp=time.time(),
                        data=LEAF_50C_data, is_extended_id=False)
      try:
          bus.send(msg)
          if bms_debug: print("Frame 50C was successful sent.")
      except can.CanError:
          print(msg, "Frame 50C could not be sent.")

      counter_100 += 1
      if counter_100 > 3:
        counter_100 = 0

      return True


    if command == "interval_10ms": # stay closed without sending periodically

      LEAF_1D4_data = [0x6E, 0x6E, 0x00, 0x04, 0x07, 0x46, 0xE0, 0x44]
      LEAF_1F2_data = [0x10, 0x64, 0x00, 0xB0, 0x00, 0x1E, 0x00, 0x8F]

      # Sending 0x1d4 message:

      if counter_10 % 4 == 0:
        LEAF_1D4_data[4] = 0x07
        LEAF_1D4_data[7] = 0x12
      if counter_10 % 4 == 1:
        LEAF_1D4_data[4] = 0x47
        LEAF_1D4_data[7] = 0xD5
      if counter_10 % 4 == 2:
        LEAF_1D4_data[4] = 0x87
        LEAF_1D4_data[7] = 0x19
      if counter_10 % 4 == 3:
        LEAF_1D4_data[4] = 0xC7
        LEAF_1D4_data[7] = 0xDE

      msg = can.Message(arbitration_id=0x1d4, timestamp=time.time(),
                        data=LEAF_1D4_data, is_extended_id=False)
      try:
          bus.send(msg)
          if bms_debug: print("Frame 1D4 was successful sent.")
      except can.CanError:
          print(msg, "Frame 1D4 could not be sent.")


      # Sending 0x1f2 message:

      if counter_10 % 20 == 0:
        LEAF_1F2_data[3] = 0xB0
        LEAF_1F2_data[6] = 0x00
        LEAF_1F2_data[7] = 0x8F
      if counter_10 % 20 == 1:
        LEAF_1F2_data[3] = 0xB0
        LEAF_1F2_data[6] = 0x01
        LEAF_1F2_data[7] = 0x80
      if counter_10 % 20 == 2:
        LEAF_1F2_data[3] = 0xB0
        LEAF_1F2_data[6] = 0x02
        LEAF_1F2_data[7] = 0x81
      if counter_10 % 20 == 3:
        LEAF_1F2_data[3] = 0xB0
        LEAF_1F2_data[6] = 0x03
        LEAF_1F2_data[7] = 0x82
      if counter_10 % 20 == 4:
        LEAF_1F2_data[3] = 0xB0
        LEAF_1F2_data[6] = 0x00
        LEAF_1F2_data[7] = 0x8F
      if counter_10 % 20 == 5:
        LEAF_1F2_data[3] = 0xB4
        LEAF_1F2_data[6] = 0x01
        LEAF_1F2_data[7] = 0x84
      if counter_10 % 20 == 6:
        LEAF_1F2_data[3] = 0xB4
        LEAF_1F2_data[6] = 0x02
        LEAF_1F2_data[7] = 0x85
      if counter_10 % 20 == 7:
        LEAF_1F2_data[3] = 0xB4
        LEAF_1F2_data[6] = 0x03
        LEAF_1F2_data[7] = 0x86
      if counter_10 % 20 == 8:
        LEAF_1F2_data[3] = 0xB4
        LEAF_1F2_data[6] = 0x00
        LEAF_1F2_data[7] = 0x83
      if counter_10 % 20 == 9:
        LEAF_1F2_data[3] = 0xB4
        LEAF_1F2_data[6] = 0x01
        LEAF_1F2_data[7] = 0x84
      if counter_10 % 20 == 10:
        LEAF_1F2_data[3] = 0xB0
        LEAF_1F2_data[6] = 0x02
        LEAF_1F2_data[7] = 0x81
      if counter_10 % 20 == 11:
        LEAF_1F2_data[3] = 0xB0
        LEAF_1F2_data[6] = 0x03
        LEAF_1F2_data[7] = 0x82
      if counter_10 % 20 == 12:
        LEAF_1F2_data[3] = 0xB0
        LEAF_1F2_data[6] = 0x00
        LEAF_1F2_data[7] = 0x8F
      if counter_10 % 20 == 13:
        LEAF_1F2_data[3] = 0xB0
        LEAF_1F2_data[6] = 0x01
        LEAF_1F2_data[7] = 0x80
      if counter_10 % 20 == 14:
        LEAF_1F2_data[3] = 0xB0
        LEAF_1F2_data[6] = 0x02
        LEAF_1F2_data[7] = 0x81
      if counter_10 % 20 == 15:
        LEAF_1F2_data[3] = 0xB4
        LEAF_1F2_data[6] = 0x03
        LEAF_1F2_data[7] = 0x86
      if counter_10 % 20 == 16:
        LEAF_1F2_data[3] = 0xB4
        LEAF_1F2_data[6] = 0x00
        LEAF_1F2_data[7] = 0x83
      if counter_10 % 20 == 17:
        LEAF_1F2_data[3] = 0xB4
        LEAF_1F2_data[6] = 0x01
        LEAF_1F2_data[7] = 0x84
      if counter_10 % 20 == 18:
        LEAF_1F2_data[3] = 0xB4
        LEAF_1F2_data[6] = 0x02
        LEAF_1F2_data[7] = 0x85
      if counter_10 % 20 == 19:
        LEAF_1F2_data[3] = 0xB4
        LEAF_1F2_data[6] = 0x03
        LEAF_1F2_data[7] = 0x86

      msg = can.Message(arbitration_id=0x1f2, timestamp=time.time(),
                        data=LEAF_1F2_data, is_extended_id=False)
      try:
          bus.send(msg)
          if bms_debug: print("Frame 1F2 was successful sent.")
      except can.CanError:
          print(msg, "Frame 1F2 could not be sent.")

      counter_10 += 1
      if counter_10 > 99:
        counter_10 = 0

      return True

    print("No such command:",command,", sorry")
    return False
