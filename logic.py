#!/usr/bin/env python

# 2023 Per Carlen
#
# This is the logic for giving info to the inverter from the perspective of faking a BYD battery

import json
import struct
import re
import time

force_load_reason = ""
charge_limit_now = -1
discharge_limit_now = -1

def Str2Int32(s):
    a1 = ord(s[0:1])
    if len(s) == 2:
        a2 = ord(s[1:2])
    else:
        a2 = 0
    ret = a1*256 + a2
    return ret


def convert2unsignedint16(signed_value):
  if signed_value < 0:
    return (65535 + signed_value)
  else:
    return signed_value


def init_inverter_data(bms_data):

    must_have_keys = ['static_nominal_capacity','static_max_voltage','static_min_voltage','static_max_power']

    static_values = {}

    if all(k in bms_data for k in must_have_keys):
      static_nominal_capacity = int(bms_data['static_nominal_capacity'])
      static_max_power = bms_data['static_max_power']
      static_max_voltage = bms_data['static_max_voltage']
      static_min_voltage = bms_data['static_min_voltage']
      static_values = {'static_nominal_capacity' : static_nominal_capacity,'static_max_power' : static_max_power,'static_max_voltage' : static_max_voltage, 'static_min_voltage' : static_min_voltage }
    json_string = json.dumps( static_values , separators=(',', ':'))
    return json_string


def update_inverter_data_from_bms(bms_data,inverter_data,logic_config):

  # Before updating, we should also check things like individual cell values and if we receive new data from can
  # also check if we need to force charge when soc is really low
  global force_load_reason
  global discharge_limit_now
  global charge_limit_now

  try:
    test = inverter_data['static_nominal_capacity']
  except:
    return

  stat_min_cell_u = bms_data['stat_min_cell_u']
  stat_max_cell_u = bms_data['stat_max_cell_u']
  stat_min_cell_t = bms_data['stat_min_cell_t']
  stat_max_cell_t = bms_data['stat_max_cell_t']
  static_min_cell_voltage = bms_data['static_min_cell_voltage'] 
  static_max_cell_voltage = bms_data['static_max_cell_voltage'] 
  static_nominal_capacity = bms_data['static_nominal_capacity']
  static_max_power = bms_data['static_max_power']
  static_max_voltage = bms_data['static_max_voltage']
  static_min_voltage = bms_data['static_min_voltage']
  stat_soc = bms_data['stat_soc']
  if 'stat_u_soc' in bms_data and 'use_voltage_based_soc' in logic_config:
    if logic_config['use_voltage_based_soc']:
      stat_soc = bms_data['stat_u_soc']
  try:
    stat_soh = bms_data['stat_soh']
  except:
    try:
      stat_soh = bms_data['stat_nominal_full_pack_energy'] / bms_data['stat_bat_beginning_of_life'] * 100
    except:
      stat_soh = 66.6
      pass
    pass
  stat_batt_voltage = bms_data['stat_batt_voltage']
  stat_batt_power = bms_data['stat_batt_power']
  # If (Tesla) bms is lost, it may report a high current/power
  if stat_batt_power > 65535: 
    stat_batt_power = 0
  target_charge_current = bms_data['target_charge_current']
  target_discharge_current = bms_data['target_discharge_current']

  if discharge_limit_now < 0: discharge_limit_now = target_discharge_current
  if charge_limit_now < 0: charge_limit_now = target_charge_current

  mode_now = 128
  try:
    mode_now = inverter_data['mode'] & 240 # remove the charge/discharge bits
  except:
    pass
  mode = 128 # This is normal...

  status = 3 # OK
  try:
    status = inverter_data['status']
  except:
    pass

  state = "idle"

  target_discharge = discharge_limit_now
  target_charge = charge_limit_now

  if target_charge == 0 and target_discharge == 0:
    target_charge = target_charge_current
    target_discharge = target_discharge_current
    print("RESET")

  if int(stat_batt_power) < -9:
    state = "discharging"
    charging = 2
    # Reset charge current limit if discharging more than selfconsumption
    if int(stat_batt_power) < -100:
      target_charge = target_charge_current
  if int(stat_batt_power) > 9:
    state = "charging"
    charging = 1
    # Reset discharge current limit if charging and not coming directly from force_charging
    if mode == 128 and force_load_reason == "":
      target_discharge = target_discharge_current
  if state == "idle":
    charging = 0
    if mode == 128:
      force_load_reason = ""
      # Avoid discharge if in idle state and cell voltage is low and limit is already in place
      if (stat_min_cell_u > static_min_cell_voltage + logic_config['stop_discharge_at_voltage'] or 'ignore_minimum_voltage' in logic_config) and stat_soc > logic_config['stop_discharge_at_soc'] and discharge_limit_now > 0 :
        target_discharge = target_discharge_current
      else:
        target_discharge = 0
      # Avoid charge if in idle state and cell voltage is high and limit is already in place
      if stat_max_cell_u < static_max_cell_voltage +  logic_config['stop_charge_at_voltage'] and stat_soc < logic_config['stop_charge_at_soc'] and charge_limit_now > 0:
        target_charge = target_charge_current
      else:
        target_charge = 0

  if mode_now == 144 and force_load_reason != "": # Forced charging
    mode = 144
  if stat_soc <= logic_config['forced_charging_soc_low']:
    force_load_reason = "low_soc"
    mode = 144
  if stat_min_cell_u <= static_min_cell_voltage + logic_config['forced_charging_cell_voltage_low'] and not 'ignore_minimum_voltage' in logic_config:
    force_load_reason = "low_voltage"
    mode = 144
  if stat_soc >= logic_config['forced_charging_soc_high'] and force_load_reason == "low_soc" and mode_now == 144:
    mode = 128
    force_load_reason = "was_in_force_charging"
  if stat_min_cell_u >= static_min_cell_voltage + logic_config['forced_charging_cell_voltage_high'] and force_load_reason == "low_voltage" and mode_now == 144:
    mode = 128
    force_load_reason = "was_in_force_charging"

  if mode_now == 144 or mode == 144 or force_load_reason !="":
    target_discharge = 0

  if status < 3: #Startup - reset targets
    target_charge = target_charge_current
    target_discharge = target_discharge_current

  if "static_charge_and_discharge_values" in logic_config:
    if logic_config["static_charge_and_discharge_values"]:
      print("WARNING!!! Using static charge and discharge value, fix the problem and set 'static_charge_and_discharge_values' in config.yaml")
      target_charge = target_charge_current
      target_discharge = target_discharge_current

  if mode == 128 and mode_now == 128: # Introduce a delay between force charge and possible discharge
    diff_cell = abs(stat_max_cell_u - stat_min_cell_u)
    if 'reduce_current_at_diff' in logic_config:
      reduce_current_at_diff = logic_config['reduce_current_at_diff']
    else:
      reduce_current_at_diff = 0.15
    if state == "discharging":
     if diff_cell > reduce_current_at_diff: #Limit current when cells differ too much
        if 'reduced_current' in logic_config:
          target_discharge = logic_config['reduced_current']
        else:
          target_discharge = 2 #Limit to 2A
     if stat_min_cell_u < static_min_cell_voltage + logic_config['reduce_discharge_current_at_voltage']: #Discharging and a cell is close to min voltage
        if 'reduced_current' in logic_config:
          target_discharge = logic_config['reduced_current']
        else:
          target_discharge = 4 #Limit to 4A
     if stat_min_cell_u < static_min_cell_voltage + logic_config['stop_discharge_at_voltage'] or stat_soc < logic_config['stop_discharge_at_soc']:
       target_discharge = 0 #Limit to 0A, if already limited

    if state == "charging":
      if diff_cell > reduce_current_at_diff: #Limit current when cells differ too much
        if 'reduced_current' in logic_config:
          target_charge = logic_config['reduced_current']
        else:
          target_charge = 2 #Limit to 2A
      if stat_max_cell_u > static_max_cell_voltage + logic_config['reduce_charge_current_at_voltage']: #Charging and a cell is close to max voltage
        if 'reduced_current' in logic_config:
          target_charge = logic_config['reduced_current']
        else:
          target_charge = 4 #Limit to 4A
      if stat_max_cell_u > static_max_cell_voltage + logic_config['stop_charge_at_voltage'] or stat_soc > logic_config['stop_charge_at_soc']:
        target_charge = 0 #Limit to 0A, if already limited

  if logic_config['logic_debug']:
    mode_str = ""
    if mode == 128: mode_str = "Normal"
    if mode == 144: mode_str = "Force load"
    print(f"Target charge I: {target_charge}A, discharge I: {target_discharge}A, Actual P: {int(stat_batt_power)}W , Umin: {stat_min_cell_u}, soc: {stat_soc}%, soh: {stat_soh}%, mode: {mode_str} (b:{mode_now}), reason:{force_load_reason}")

  if status == 3:
    new_mode = mode + charging
  else:
    new_mode = mode

  target_discharge_power = int(target_discharge * stat_batt_voltage)
  target_charge_power = int(target_charge * stat_batt_voltage)

  inv_dict = { 'status':status,'mode':new_mode, 'soc':stat_soc, 'soh':stat_soh, 'static_nominal_capacity':static_nominal_capacity,'target_discharge_power': target_discharge_power, 'target_charge_power': target_charge_power,
    'batt_voltage': stat_batt_voltage,'batt_power':stat_batt_power, 'cell_temp_min':stat_min_cell_t, 'cell_temp_max':stat_max_cell_t,
    'cell_voltage_min':stat_min_cell_u, 'cell_voltage_max':stat_max_cell_u  }

  inverter_data.update(inv_dict)

  discharge_limit_now = target_discharge
  charge_limit_now = target_charge

  return inverter_data


# Got MQTT message
def got_message(topic,payload,bms_data,inverter_data,logic_config,bms_type,mqtt_client):

  # Make sure we have everything we need before populating the registers
  must_have_keys = ['static_nominal_capacity','static_min_cell_voltage','static_max_cell_voltage','static_max_voltage','static_min_voltage',
  'stat_soc','stat_batt_voltage','stat_batt_current','stat_batt_power','target_charge_current','target_discharge_current','static_max_power','last_updated_soc',
  'stat_min_cell_u','stat_max_cell_u','stat_min_cell_t','stat_max_cell_t'
  ]
  if all(k in bms_data for k in must_have_keys) :
    if logic_config['logic_debug']: print("all keys in place")
    time_now = time.time()
    if logic_config['logic_debug']: print("Received message on topic '" + topic )
    # This is normal update of modbus registers from bms-data
    inverter_data = update_inverter_data_from_bms(bms_data,inverter_data,logic_config)
    if inverter_data:
     if topic == "inverter/writes": # OK, let's check if state needs to be modified etc....
      json_data = json.loads(payload)
      if 'r401' in json_data: # Change state, startup....
        data2write = json_data['r401']
        if inverter_data['status'] == 0: # time to start....
            inverter_data['status'] = 1
        if inverter_data['status'] == 4: # Fault - try to restart....
            inverter_data['status'] = 1
        if data2write[0] == 1 and inverter_data['status'] != 3: # time to close contactor if we have one
            ok_to_transition = False
            if bms_type == "batrium":
                ok_to_transition = True
            if bms_type == "tesla_model3":
                ok_to_transition = True
            if ok_to_transition:
                if logic_config['logic_debug']: print("Transitioning to active state")
                inverter_data['status'] = 3
            else:
                inverter_data['status'] = 1
        if data2write[0] == 2 and inverter_data['status'] != 3:
            inverter_data['status'] = 3
#        for index,value in enumerate(data2write):
#            p401[index] = value
      #p301_dict = { 'r301':p301 }
      #inverter_data.update(p301_dict)
      #p401_dict = { 'r401':p401 }
      #inverter_data.update(p401_dict)

     inverter_data['last_updated'] = time_now
     inverter_data['last_updated_soc'] = bms_data['last_updated_soc']
     return inverter_data
  else:
    missing = ""
    for k in must_have_keys:
      if not k in bms_data:
        missing += k + ","
    print(" Not updating since not all bms or inverter data is in place, missing: ", missing)
  return False
