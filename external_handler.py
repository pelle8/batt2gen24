#!/usr/bin/env python
# 2023 Per Carlén
#
# This is a handler for "external", which could be something like usb-relays or contactor-control via gpio
# Subscribes on control commands that will be sent to vms
#  test pub with: $ mosquitto_pub -h localhost -m "close_relay" -t "external/control" -u "batt2gen24" -P "batt2gen24_pass"
#
#

import time
import json
import yaml
import re
import paho.mqtt.client as paho
import asyncio
from serial import Serial
import os
import subprocess
import psutil
import signal
import RPi.GPIO as GPIO
import time

# Contactor control:
# GPIO 12 = RPi pin 32 - pin_neg_contactor
# GPIO 13 = RPi pin 33 - pin_pos_contactor
# GPIO 19 = RPi pin 35 - pin_precharge


def read_config_file():

    config_json = ""
    filename = "config.yaml"
    path = "/etc/batt2gen24/"
    if os.path.isfile(path+filename):
        filename = path + filename
    print("reading from:",filename)
    try:
      with open(filename,'r') as file:
        config_json = json.dumps(yaml.safe_load(file))
    except:
      print("ERROR: No config file - ",filename)
      exit(1)

    return config_json

def mqtt_on_publish(mqtt_client,userdata,result): 
    #print("data published:",result)
    pass

def mqtt_on_message(mqtt_client,userdata, message,tmp=None):
    global external_debug
    global external_relays
    global contactors_requested_state
    global external_state

    if external_debug:
      pass
      #print(" Received message " + str(message.payload)
      #+ " on topic '" + message.topic
      #+ "' with QoS " + str(message.qos))
    if message.topic == "external/control":
      try:
        json_data = json.loads(message.payload)
        if external_debug: print("Got MQTT json:",json_data)
      except:
        print("Error ocurred during mqtt message decoding")
        pass
      if 'type' in json_data:
        if json_data['type'] == 'relay':
          relay_id = json_data['id']
          state = json_data['state']
          for r in external_relays:
            if r['id'] == relay_id:
              if external_debug: print("Relay",r['device'],state)
              if 'tty' in r['device']:
                serial_port = Serial(r['device'], r['device_speed'])
                if state == "on":
                  command = r['close_relay_command']
                if state == "off":
                  command = r['open_relay_command']
                serial_port.write(bytearray(command.encode('utf-8')))
              if 'USBRelay' in r['device']:
                if state == "on":
                  os.system(r['close_relay_command'])
                  contactors_requested_state = "close"
                  external_state = { 'contactors_state': 'closed' }
                if state == "off":
                  contactors_requested_state = "open"
                  os.system(r['open_relay_command'])
                  external_state = { 'contactors_state': 'open' }
        if json_data['type'] == 'contactors' and json_data['command'] == 'close':
          contactors_requested_state = "close"
        if json_data['type'] == 'contactors' and json_data['command'] == 'open':
          contactors_requested_state = "open"


def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    pass

def mqtt_on_connect(client, userdata, flags, rc):
    if rc != 0:
      print("MQTT connection problem")
      client.connected_flag=False
    else:
      print("MQTT client connected:" + str(client))
      client.connected_flag=True
      client.subscribe("external/control",2)

def publish(client,pub_var,topic):
    json_string = json.dumps(pub_var)
    ret = client.publish(topic,json_string) 


async def main() -> None:
    global mqtt_client1
    global mqtt_client2
    global external_debug
    global external_relays
    global external_state
    global config
    global contactors_requested_state


    print("\nStarting external_handler")
    print("Reading config from file")

    external_debug = 0
    #signal.signal(signal.SIGCHLD, signal.SIG_IGN)
    external_state = { "contactors_state": 'open' }
    contactors_requested_state = ""

    config = json.loads(read_config_file())
    external_relays = []
    external_contactors = []
    if config:
      if 'external_relays' in config: external_relays = config['external_relays'] 
      if 'external_contactors' in config: external_contactors = config['external_contactors']
      external_debug = config['external_debug']
      mqtt_broker = config['mqtt_broker']
      mqtt_port = config['mqtt_port']
      mqtt_username = config['mqtt_username']
      mqtt_password = config['mqtt_password']
    else:
      exit(1)

    # this mqtt client will publish, what...state?
    mqtt_client1 = paho.Client(client_id="external_handler_pub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client1.on_publish = mqtt_on_publish
    mqtt_client1.on_connect = mqtt_on_connect
    mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client1.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client1.loop_start()

    # this mqtt client will subscribe
    mqtt_client2 = paho.Client(client_id="external_handler_sub",transport="tcp",protocol=paho.MQTTv311,clean_session=True)  
    mqtt_client2.on_subscribe = mqtt_on_subscribe
    mqtt_client2.on_connect = mqtt_on_connect
    mqtt_client2.on_message = mqtt_on_message
    mqtt_client2.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client2.connect(mqtt_broker,mqtt_port,keepalive=60)
    mqtt_client2.loop_start()

    if 'external_contactors' in config:
      pin_neg_contactor = 12
      pin_pos_contactor = 13
      pin_precharge = 19
      GPIO.setwarnings(False)
      GPIO.setmode(GPIO.BCM)		#set pin numbering system
      GPIO.setup(pin_neg_contactor,GPIO.OUT)
      GPIO.setup(pin_pos_contactor,GPIO.OUT)
      GPIO.setup(pin_precharge,GPIO.OUT)
      GPIO.output(pin_neg_contactor, GPIO.LOW)
      GPIO.output(pin_pos_contactor, GPIO.LOW)
      GPIO.output(pin_precharge, GPIO.LOW)
      external_state = { "contactors_state": 'open' }
   

    try:
        while True:
          if 'external_contactors' in config:
            if external_state['contactors_state'] == "open" and contactors_requested_state == "close":
              time.sleep(1)
              # Precharge on
              GPIO.output(pin_precharge, GPIO.HIGH)
              time.sleep(external_contactors['time_precharge'])

              if external_contactors['pwm'] < 100: # Let's do PWM
                # Neg on
                pi_pwm_neg = GPIO.PWM(pin_neg_contactor,external_contactors['frequency'])		#create PWM instance with frequency
                pi_pwm_neg.start(0) 			#start PWM of required Duty Cycle 
                pi_pwm_neg.ChangeDutyCycle(100) 			#start PWM of required Duty Cycle 
                time.sleep(external_contactors['time_contactor_on_before_pwm'])
                # Economize
                pi_pwm_neg.ChangeDutyCycle(external_contactors['pwm'])

                # Pos on
                pi_pwm_pos = GPIO.PWM(pin_pos_contactor,external_contactors['frequency'])		#create PWM instance with frequency
                pi_pwm_pos.start(0) 			#start PWM of required Duty Cycle 
                pi_pwm_pos.ChangeDutyCycle(100) 			#start PWM of required Duty Cycle 
                time.sleep(external_contactors['time_contactor_on_before_pwm'])
                # Economize
                pi_pwm_pos.ChangeDutyCycle(external_contactors['pwm'])
              else: # No PWM
                GPIO.output(pin_neg_contactor, GPIO.HIGH)
                time.sleep(external_contactors['time_contactor_on_before_pwm'])
                GPIO.output(pin_pos_contactor, GPIO.HIGH)
                time.sleep(external_contactors['time_contactor_on_before_pwm'])

              time.sleep(external_contactors['time_precharge'])
              # Precharge off
              GPIO.output(pin_precharge, GPIO.LOW)
              external_state = { 'contactors_state': 'closed' }
              if external_contactors['pwm'] < 100:
                external_state = { 'contactors_state': 'economized' }

            if external_state['contactors_state'] != "open" and contactors_requested_state == "open":
              if external_contactors['pwm'] < 100: # Let's do PWM
                pi_pwm_pos.ChangeDutyCycle(100) 			#start PWM of required Duty Cycle 
                pi_pwm_neg.ChangeDutyCycle(100) 			#start PWM of required Duty Cycle 
                pi_pwm_pos.stop()
                pi_pwm_neg.stop()
                time.sleep(0.5)
              GPIO.output(pin_neg_contactor, GPIO.LOW)
              GPIO.output(pin_pos_contactor, GPIO.LOW)
              external_state = { 'contactors_state': 'open' }

          publish(mqtt_client1,external_state,"external/status")
          await asyncio.sleep(2)
          if external_debug: print(external_state)
    except KeyboardInterrupt:
        # Wait for last message to arrive
        await reader.get_message()
        print("Done!")

        # Clean-up
        notifier.stop()
        mqtt_client1.disconnect()
        mqtt_client2.disconnect()
        pass  # exit normally


if __name__ == "__main__":
    asyncio.run(main())
