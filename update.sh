cd /opt/batt2gen24
sudo systemctl stop batt2gen24_bms_handler
sudo systemctl stop batt2gen24_inverter_handler
sudo systemctl stop batt2gen24_logic_handler
sudo systemctl stop batt2gen24_stats_collector
sudo systemctl stop batt2gen24_external_handler
sudo git stash
sudo git pull
sudo chmod +x *.sh
sudo rm /var/www/html/stats.html
sudo ln -s /opt/batt2gen24/www/stats.html /var/www/html/stats.html
sudo systemctl start batt2gen24_bms_handler
sudo systemctl start batt2gen24_inverter_handler
sudo systemctl start batt2gen24_logic_handler
sudo systemctl start batt2gen24_stats_collector
sudo systemctl start batt2gen24_external_handler