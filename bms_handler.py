#!/usr/bin/env python
"""
- Handles communications towards the BMS // Per Carlen
"""
from typing import Any, Dict
import os
import time
import json
import yaml
import copy
import threading
import importlib
from typing import Callable

class ModuleThread:
    def __init__(self, name: str, target: Callable):
        self._name = name
        self._alive = False
        self.running = False
        self._target = target
        self._thread = None

    def start_thread(self):
        if not self._alive:
            self._thread = threading.Thread(name=self._name, target=self._run, daemon=True)
            self._thread.start()

    def stop_thread(self):
        if self._alive:
            self.running = False

    def start_stop_thread(self):
        if self._alive:
            self.stop_thread()
        else:
            self.start_thread()

    def _run(self):
        self._alive = True
        self.running = True
        self._target()
        self._alive = False

    def is_alive(self):
        return self._alive


class BMSHandler:
  def __init__(self):
    print("\nStarting bms_handler")
    self.config = self.read_config_file('config.yaml')
    print("Starting thread for BMS: ",self.config['bms_type'])
    self.thread: ModuleThread = ModuleThread('bms_' + self.config['bms_type'], self.run)
    self.thread.start_thread()

  def run(self):
    while self.thread.running:
      print("Loading module for BMS: ",self.config['bms_type'])
      bms = importlib.import_module('bms_' + self.config['bms_type'], package=None)
      bms_comms = importlib.import_module('bms_comms_' + self.config['bms_type'], package=None)
      bms.run_updating_server(self,bms_comms)
      time.sleep(1)

  @staticmethod
  def read_config_file(filename: str) -> Dict:

    config = ""
    filename = "config.yaml"
    path = "/etc/batt2gen24/"
    if os.path.isfile(path+filename):
      filename = path + filename
    print("Reading from:",filename)
    try:
      with open(filename,'r') as file:
        config = yaml.safe_load(file)
    except:
      print("ERROR: No config file - ",filename)
      exit(1)
    return config

  def loop(self):
    try:
      while True:
        time.sleep(2)
    except KeyboardInterrupt:
      print("Done!")
      pass

if __name__ == '__main__':
    bms_handler = BMSHandler()
    bms_handler.loop()
